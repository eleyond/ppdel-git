from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, QFileDialog
from datetime import datetime

filename = ''

global ppdel_visible_columns
global ppdel_hidden_columns

global ppdel_visible_customers
global ppdel_hidden_customers

global ppdel_visible_orders
global ppdel_hidden_orders

ppdel_visible_orders = []
ppdel_hidden_orders= []

ppdel_visible_customers = []
ppdel_hidden_customers = []

ppdel_visible_columns = []
ppdel_hidden_columns = []

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(511, 646)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget.setPalette(palette)
        self.listWidget.setObjectName("listWidget")
        self.verticalLayout.addWidget(self.listWidget)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.listWidget_2 = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget_2.setPalette(palette)
        font = QtGui.QFont()
        font.setStrikeOut(True)
        self.listWidget_2.setFont(font)
        self.listWidget_2.setObjectName("listWidget_2")
        self.verticalLayout_2.addWidget(self.listWidget_2)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_3.addWidget(self.label_3)
        self.listWidget_3 = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget_3.setPalette(palette)
        self.listWidget_3.setObjectName("listWidget_3")
        self.verticalLayout_3.addWidget(self.listWidget_3)
        self.horizontalLayout_2.addLayout(self.verticalLayout_3)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_4.addWidget(self.label_4)
        self.listWidget_4 = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget_4.setPalette(palette)
        font = QtGui.QFont()
        font.setStrikeOut(True)
        self.listWidget_4.setFont(font)
        self.listWidget_4.setObjectName("listWidget_4")
        self.verticalLayout_4.addWidget(self.listWidget_4)
        self.horizontalLayout_2.addLayout(self.verticalLayout_4)
        self.gridLayout.addLayout(self.horizontalLayout_2, 3, 0, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_5.addWidget(self.label_5)
        self.listWidget_5 = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 85, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget_5.setPalette(palette)
        self.listWidget_5.setObjectName("listWidget_5")
        self.verticalLayout_5.addWidget(self.listWidget_5)
        self.horizontalLayout_3.addLayout(self.verticalLayout_5)
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.verticalLayout_6.addWidget(self.label_6)
        self.listWidget_6 = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget_6.setPalette(palette)
        font = QtGui.QFont()
        font.setStrikeOut(True)
        self.listWidget_6.setFont(font)
        self.listWidget_6.setObjectName("listWidget_6")
        item = QtWidgets.QListWidgetItem()
        self.listWidget_6.addItem(item)
        self.verticalLayout_6.addWidget(self.listWidget_6)
        self.horizontalLayout_3.addLayout(self.verticalLayout_6)
        self.gridLayout.addLayout(self.horizontalLayout_3, 4, 0, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 0, 0, 1, 1)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.gridLayout.addWidget(self.pushButton_2, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 511, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.pushButton.clicked.connect(self.ppdel_read_ppdel)
        self.pushButton_2.clicked.connect(self.export_to_excel_marking)

        self.listWidget.itemDoubleClicked.connect(self.ppdel_column_hide)
        self.listWidget_2.itemDoubleClicked.connect(self.ppdel_column_show)

        self.listWidget_3.itemDoubleClicked.connect(self.ppdel_customer_hide)
        self.listWidget_4.itemDoubleClicked.connect(self.ppdel_customer_show)

        self.listWidget_5.itemDoubleClicked.connect(self.ppdel_order_hide)
        self.listWidget_6.itemDoubleClicked.connect(self.ppdel_order_show)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Included columns"))
        self.label_2.setText(_translate("MainWindow", "Exclude columns"))
        self.label_3.setText(_translate("MainWindow", "Included ship to party"))
        self.label_4.setText(_translate("MainWindow", "Excluded ship to party"))
        self.label_5.setText(_translate("MainWindow", "Included ship to party"))
        self.label_6.setText(_translate("MainWindow", "Excluded ship to party"))
        __sortingEnabled = self.listWidget_6.isSortingEnabled()
        self.listWidget_6.setSortingEnabled(False)
        self.listWidget_6.setSortingEnabled(__sortingEnabled)
        self.pushButton.setText(_translate("MainWindow", "Scan PPDEL plan"))
        self.pushButton_2.setText(_translate("MainWindow", "Generate PPDEL report"))

    def ppdel_open_dialog_box(self):
        file_name = QFileDialog.getOpenFileName()
        path = file_name[0]
        print(path)

        global filename
        filename = path

    def ppdel_read_ppdel(self):
        import openpyxl
        global filename

        global ppdel_hidden_columns
        global ppdel_visible_columns

        global ppdel_hidden_customers
        global ppdel_visible_customers

        global ppdel_hidden_orders
        global ppdel_hidden_orders

        self.ppdel_open_dialog_box()
        try:
            wb = openpyxl.load_workbook(filename)
            sheet = wb.active

            ppdel_hidden_customers = []
            ppdel_hidden_columns = []
            ppdel_hidden_orders = []
            
        except:
            print('wrong file type')

        try:
            for i in range(1, 63):
                cell_value = sheet.cell(row=1, column=i).value
                print(cell_value)
                ppdel_hidden_columns.append(cell_value)
            self.ppdel_update_columns()

            for i in range(1,10000):
                cell_value = str(sheet.cell(row=i, column=53).value)
                if cell_value not in ppdel_hidden_customers:
                    ppdel_hidden_customers.append(cell_value)
                    #print(cell_value)
            ppdel_hidden_customers.sort()
            print(ppdel_hidden_customers)
            self.ppdel_update_customer()
            self.listWidget_4.addItems(ppdel_hidden_customers)

            ppdel_hidden_orders = ['Plnd order', 'Order', 'Delivery', 'ProdOrder']
            self.ppdel_update_orders()
            wb.close()

        except Exception as e:
            print(e)


    def ppdel_order_hide(self):

        global ppdel_visible_orders
        global ppdel_hidden_orders

        try:
            list_item = self.listWidget_5.currentItem().text()
            print(list_item)

            ppdel_visible_orders.remove(list_item)
            ppdel_hidden_orders.append(list_item)

        except AttributeError:
            print('hide; no item selected')

        self.ppdel_update_orders()

    def ppdel_order_show(self):

        global ppdel_visible_orders
        global ppdel_hidden_orders

        try:
            list_item = self.listWidget_6.currentItem().text()
            print(list_item)

            ppdel_visible_orders.append(list_item)
            ppdel_hidden_orders.remove(list_item)


        except AttributeError:
            print('Show, no item selected')

        self.ppdel_update_orders()

    def ppdel_update_orders(self):

        global ppdel_visible_orders
        global ppdel_hidden_orders

        self.listWidget_5.clear()
        self.listWidget_6.clear()

        self.listWidget_5.addItems(ppdel_visible_orders)
        self.listWidget_6.addItems(ppdel_hidden_orders)


    def ppdel_customer_hide(self):

        global ppdel_visible_customers
        global ppdel_hidden_customers

        try:
            list_item = self.listWidget_3.currentItem().text()
            print(list_item)

            ppdel_visible_customers.remove(list_item)
            ppdel_hidden_customers.append(list_item)

        except AttributeError:
            print('hide; no item selected')

        self.ppdel_update_customer()

    def ppdel_customer_show(self):

        global ppdel_visible_customers
        global ppdel_hidden_customers

        try:
            list_item = self.listWidget_4.currentItem().text()
            print(list_item)

            ppdel_visible_customers.append(list_item)
            ppdel_hidden_customers.remove(list_item)


        except AttributeError:
            print('Show, no item selected')

        self.ppdel_update_customer()

    def ppdel_update_customer(self):

        global ppdel_visible_customers
        global ppdel_hidden_customers

        self.listWidget_3.clear()
        self.listWidget_4.clear()

        self.listWidget_3.addItems(ppdel_visible_customers)
        self.listWidget_4.addItems(ppdel_hidden_customers)


    def ppdel_column_hide(self):

        global ppdel_visible_columns
        global ppdel_hidden_columns

        try:
            list_item = self.listWidget.currentItem().text()
            print(list_item)

            ppdel_visible_columns.remove(list_item)
            ppdel_hidden_columns.append(list_item)

        except AttributeError:
            print('hide; no item selected')

        self.ppdel_update_columns()

    def ppdel_column_show(self):

        global ppdel_visible_columns
        global ppdel_hidden_columns

        try:
            list_item = self.listWidget_2.currentItem().text()
            print(list_item)

            ppdel_visible_columns.append(list_item)
            ppdel_hidden_columns.remove(list_item)


        except AttributeError:
            print('Show, no item selected')

        self.ppdel_update_columns()

    def ppdel_update_columns(self):

        global ppdel_visible_columns
        global ppdel_hidden_columns

        self.listWidget.clear()
        self.listWidget_2.clear()

        self.listWidget.addItems(ppdel_visible_columns)
        self.listWidget_2.addItems(ppdel_hidden_columns)


    def export_to_excel_marking(self):

        import openpyxl
        global filename

        global ppdel_hidden_columns
        global ppdel_visible_columns

        global ppdel_hidden_customers
        global ppdel_visible_customers

        global ppdel_hidden_orders
        global ppdel_hidden_orders

        wb = openpyxl.load_workbook('templatePPDEL.xlsx')
        sheet = wb.active

        wb2 = openpyxl.load_workbook(filename)
        sheet2 = wb2.active



        now = datetime.now()
        dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")

        sheet.cell(row=1, column=1).value = 'Generated by ' + 'Name of project coordinator'
        sheet.cell(row=2, column=1).value = 'Generated on ' + dt_string

        rowcounter = 7
        colcounter = 0


        # Add the relevant column titles
        for j in range(1, 63):
            column_title = sheet2.cell(row=1, column=j).value

            if column_title in ppdel_visible_columns:
                colcounter += 1
                sheet.cell(row=(rowcounter), column=colcounter).value = sheet2.cell(row=1, column=j).value
        rowcounter +=1


        for i in range(1, 10000):

            # Is the ship to party field in the current row equal to one of the customers we want to report on.
            cell_value = str(sheet2.cell(row=i, column=53).value)
            if cell_value in ppdel_visible_customers:
                if sheet2.cell(row=i, column=5).value in ppdel_visible_orders:
                    colcounter = 0
                    rowcounter += 1
                    # print the name of the customer
                    print(cell_value)

                    # loop through the entire row.
                    for j in range(1, 63):
                        # Check if the current cell is in a column, which is part of the list of colums we wish to report on.
                        column_title = sheet2.cell(row=1, column=j).value
                        print(column_title)
                        if column_title in ppdel_visible_columns:
                            print(column_title)
                            colcounter += 1
                            # If the order type matches


                            # Write to the new report file
                            sheet.cell(row=rowcounter, column=colcounter).value = sheet2.cell(row=i, column = j).value



        saveFileName = ' generated PPDEL REPORT' + str(dt_string) + '.xlsx'
        print(saveFileName)

        wb.save(saveFileName)
        wb.close()
        wb2.close()

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

